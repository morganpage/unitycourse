﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace Dodgey
{

    public class GameManager : MonoBehaviour
    {
        public Vector2 m_MaxBounds;
        public GameObject m_GameOver;

        void Start()
        {
            Time.timeScale = 1;
            m_GameOver.SetActive(false);
        }

        public void GameOver()
        {
            m_GameOver.SetActive(true);
            Time.timeScale = 0;
        }

        void Update()
        {
            if (m_GameOver.activeSelf)
            {
                if (Input.anyKeyDown)
                {
                    Start();
                }
            }
        }
    }
}