﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dodgey
{

    public class Baddy : MonoBehaviour
    {
        private float m_Speed = 4;
        private Vector2 m_MaxBounds;

        void Start()
        {
            m_MaxBounds = FindObjectOfType<GameManager>().m_MaxBounds;
            transform.position = new Vector2(Random.Range(-m_MaxBounds.x, m_MaxBounds.x), m_MaxBounds.y);
        }

        // Update is called once per frame
        void Update()
        {
            transform.Translate(new Vector2(0, -1) * Time.deltaTime * m_Speed);
            if (transform.position.y < -m_MaxBounds.y) Start();
        }
    }

}
