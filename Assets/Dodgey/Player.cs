﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Dodgey
{

    public class Player : MonoBehaviour
    {
        public float m_Speed = 6;
        private Vector2 m_MaxBounds;

        // Use this for initialization
        void Start()
        {
            m_MaxBounds = FindObjectOfType<GameManager>().m_MaxBounds;
        }

        // Update is called once per frame
        void Update()
        {
            Vector2 moveInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
            moveInput.Normalize();
            transform.Translate(moveInput * Time.deltaTime * m_Speed);

            Vector2 playerPosition = transform.position;

            if (playerPosition.x > m_MaxBounds.x) playerPosition.x = m_MaxBounds.x;
            if (playerPosition.x < -m_MaxBounds.x) playerPosition.x = -m_MaxBounds.x;
            if (playerPosition.y > m_MaxBounds.y) playerPosition.y = m_MaxBounds.y;
            if (playerPosition.y < -m_MaxBounds.y) playerPosition.y = -m_MaxBounds.y;

            transform.position = playerPosition;
        }

        void OnTriggerEnter2D(Collider2D other)
        {
            Debug.Log("OnTriggerEnter2D");
            FindObjectOfType<GameManager>().GameOver();
        }


    }

}
