﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.VR;

public class VREnabler : NetworkBehaviour {
    public enum VREnableTypes { Server, Client, Both}
    public VREnableTypes m_VREnableType;
    private string m_VRDevice = "OpenVR";

    void OnDisable()
    {
        VRSettings.enabled = false;
    }

    public override void OnStartLocalPlayer()
    {
        switch (m_VREnableType)
        {
            case VREnableTypes.Server:
                if (isServer)
                {
                    StartCoroutine(LoadDevice(m_VRDevice));
                }
                else
                {
                    VRSettings.enabled = false;
                }
                break;
            case VREnableTypes.Client:
                if (isClient)
                {
                    StartCoroutine(LoadDevice(m_VRDevice));
                }
                else
                {
                    VRSettings.enabled = false;
                }
                break;
            default:
                StartCoroutine(LoadDevice(m_VRDevice));
                break;
        }
    }

    IEnumerator LoadDevice(string newDevice)
    {
        Debug.Log("VR Enabled: " + newDevice);
        VRSettings.LoadDeviceByName(newDevice);
        yield return null;
        VRSettings.enabled = true;
    }
}
