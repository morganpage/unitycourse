﻿using UnityEngine;
using UnityEngine.Networking;

public class PlayerController : NetworkBehaviour
{
    public string m_Horizontal = "Horizontal";
    public string m_Vertical = "Vertical";

    void Update()
    {
        if (!isLocalPlayer) return;

        var x = Input.GetAxis(m_Horizontal) * Time.deltaTime * 150.0f;
        var z = Input.GetAxis(m_Vertical) * Time.deltaTime * 3.0f;

        transform.Rotate(0, x, 0);
        transform.Translate(0, 0, z);
    }
}