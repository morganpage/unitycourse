﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class VRPlayer : NetworkBehaviour {

    public override void OnStartLocalPlayer()
    {
        Debug.Log("VRPlayer");
        //If using motion controllers make CameraRig child of player
        SteamVR_ControllerManager steamVR_ControllerManager = GameObject.FindObjectOfType<SteamVR_ControllerManager>();
        if (steamVR_ControllerManager)
        {
            steamVR_ControllerManager.transform.parent = transform;
            steamVR_ControllerManager.transform.localPosition = Vector3.zero;
            steamVR_ControllerManager.transform.localRotation = Quaternion.identity;
        }
        else
        {   //If not using motion controlles, make VRCamera child of player
            Transform camera = GameObject.FindWithTag("VRCamera").transform;
            camera.parent = transform;
            camera.localPosition = new Vector3(0, 1.6f, 0);
            camera.localRotation = Quaternion.identity;
        }

    }
}
