﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class VRNetworkManager : NetworkManager
{
    public GameObject[] playerCharacterPrefabs;
    private Transform m_SpawnPoint;

    public override void OnStartClient(NetworkClient client)
    {
        base.OnStartClient(client);
        foreach (GameObject go in playerCharacterPrefabs) ClientScene.RegisterPrefab(go);
    }

    public override void OnClientConnect(NetworkConnection conn)
    {
        base.OnClientConnect(conn);
        for (short i = 1; i < playerCharacterPrefabs.Length; i++)
        {
            Debug.Log("OnClientConnect:" + client.connection.address);
            ClientScene.AddPlayer(client.connection, i);
        }
    }

    public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
    {
        Debug.Log("OnServerAddPlayer:" + playerControllerId);
        GameObject newPlayer = GameObject.Instantiate(playerCharacterPrefabs[playerControllerId]);
        if (playerControllerId == 0) m_SpawnPoint = GetStartPosition();
        newPlayer.transform.position = m_SpawnPoint.position;
        newPlayer.transform.rotation = m_SpawnPoint.rotation;
        NetworkServer.Spawn(newPlayer);
        NetworkServer.AddPlayerForConnection(conn, newPlayer, playerControllerId);
    }

}
