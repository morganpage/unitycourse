﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Health : NetworkBehaviour {
    public const int m_MaxHealth = 100;

    [SyncVar(hook = "OnChangeHealth")]
    public int m_CurrentHealth = m_MaxHealth;

    public event Action<float,float> OnHealthChanged;
    private Vector3 m_StartPosition;
    private Quaternion m_StartRotation;

    public override void OnStartLocalPlayer()
    {
        m_StartPosition = transform.position;
    }

    public void TakeDamage(int amount)
    {
        if (!isServer) return;
        m_CurrentHealth -= amount;
        if (m_CurrentHealth <= 0)
        {
            m_CurrentHealth = 100;
            RpcRespawn();// called on the Server, but invoked on the Clients
        }
    }

    void OnChangeHealth(int health)
    {
        if (OnHealthChanged != null) OnHealthChanged(health, m_MaxHealth);
    }

    [ClientRpc]
    void RpcRespawn()
    {
        if (isLocalPlayer)//Client has authority so this must only be done by local player
        {
            transform.position = m_StartPosition;
            transform.rotation = m_StartRotation;
        }
    }
}
