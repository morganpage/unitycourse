﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class VRHead : NetworkBehaviour {

    public override void OnStartLocalPlayer()
    {
        Debug.Log("VRHead");
        GameObject camera = GameObject.FindWithTag("VRCamera");
        transform.parent = camera.transform;
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;

        gameObject.layer = LayerMask.NameToLayer("Head");
        foreach (Transform t in transform)
        {
            t.gameObject.layer = LayerMask.NameToLayer("Head");//So player cant see own head
        }

    }
}
