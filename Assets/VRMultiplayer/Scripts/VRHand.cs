﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class VRHand : NetworkBehaviour {
    public bool m_RightHand;
    

    public override void OnStartLocalPlayer()
    {
        Debug.Log("VRHand");
        //If using motion controllers make hand child of controller
        SteamVR_ControllerManager steamVR_ControllerManager = GameObject.FindObjectOfType<SteamVR_ControllerManager>();
        if (steamVR_ControllerManager)
        {
            if (m_RightHand)
            {
                transform.parent = steamVR_ControllerManager.right.transform;
                
            }
            else
            {
                transform.parent = steamVR_ControllerManager.left.transform;
            }
                

            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
        }
        

    }

    
    

}
