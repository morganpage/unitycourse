﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour {
    public Transform m_Bar;
    public float m_Scale;
    public Health m_Health;

    void OnEnable()
    {
        if(m_Health) m_Health.OnHealthChanged += UpdateHealthBar;
    }
    void OnDisable()
    {
        if (m_Health) m_Health.OnHealthChanged -= UpdateHealthBar;
    }

    void UpdateHealthBar(float current,float max)
    {
        m_Scale = current / max;
        m_Bar.localScale = new Vector3(m_Scale, 1, 1);
        m_Bar.gameObject.SetActive(m_Scale > 0);
    }
	

}
