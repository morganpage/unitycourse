﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class VRGun : NetworkBehaviour
{
    public Transform m_ProjectileSpawn;
    public GameObject m_ProjectilePrefab;
    public float m_ProjectileDestroyDelay;
    private SteamVR_TrackedController m_trackedController;
    
    public override void OnStartLocalPlayer()
    {
        Debug.Log("VRGun");
        VRHand vrhand = GetComponent<VRHand>();
        SteamVR_ControllerManager steamVR_ControllerManager = GameObject.FindObjectOfType<SteamVR_ControllerManager>();
        
        if (steamVR_ControllerManager && vrhand)
        {
            Transform controller;
            if (vrhand.m_RightHand)
                controller = steamVR_ControllerManager.right.transform;
            else
                controller = steamVR_ControllerManager.left.transform;

            m_trackedController = controller.GetComponent<SteamVR_TrackedController>();
            if (m_trackedController == null)
            {
                m_trackedController = controller.gameObject.AddComponent<SteamVR_TrackedController>();
            }
            m_trackedController.TriggerClicked += new ClickedEventHandler(DoTriggerClicked);
        }
    }

    void DoTriggerClicked(object sender, ClickedEventArgs e)
    {
        if (!isLocalPlayer) return;
        CmdFire();
    }

    [Command]
    void CmdFire()
    {
        if (m_ProjectileSpawn)
        {

            GameObject projectile = (GameObject)Instantiate(m_ProjectilePrefab, m_ProjectileSpawn.position, m_ProjectileSpawn.rotation);
            // spawn the bullet on the clients
            NetworkServer.Spawn(projectile);

            // make projectile disappear after m_DestroyDelay seconds
            if (m_ProjectileDestroyDelay != 0) Destroy(projectile, m_ProjectileDestroyDelay);
        }
    }
}
