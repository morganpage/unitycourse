﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LevelElement //defines each item in a level by mapping a single character(ie #) to a prefab
{
    public string m_Character;
    public GameObject m_Prefab;
}

public class LevelBuilder : MonoBehaviour {//Builds the level from its definition
    public int m_CurrentLevel;
    public List<LevelElement> m_LevelElements;
    public bool m_YisZ;//Use x,z instead of x,y - good for isometric/3D games
    public bool m_Offset;//Offset start co-ordinates so that 0,0 is centre
    private Level m_Level;

    GameObject GetPrefab(char c)
    {
        LevelElement levelElement = m_LevelElements.Find(le => le.m_Character == c.ToString());
        if (levelElement != null)
            return levelElement.m_Prefab;
        else
            return null;
    }

    public void NextLevel()
    {
        m_CurrentLevel++;
        if(m_CurrentLevel >= GetComponent<Levels>().m_Levels.Count)
        {
            m_CurrentLevel = 0;//Wrap back to first level
        }
    }

	public void Build() {
        m_Level = GetComponent<Levels>().m_Levels[m_CurrentLevel];
        int startx=0, x=0, y=0;
        if (m_Offset)//Offset coordinates so that centre of level is roughly at 0,0
        {
            startx = -m_Level.Width / 2;//Save start x since needs to be reset in loop
            x = startx;
            y = -m_Level.Height / 2;
        }
        foreach(var row in m_Level.m_Rows)
        {
            foreach (var ch in row)
            {
                Debug.Log(ch);
                GameObject prefab = GetPrefab(ch);
                if (prefab)
                {
                    Debug.Log(prefab.name);
                    if(m_YisZ)
                        Instantiate(prefab,new Vector3(x,0,y),Quaternion.identity);
                    else
                        Instantiate(prefab, new Vector3(x, y, 0), Quaternion.identity);
                }
                x++;
            }
            y++;
            x = startx;
        }
    }
	

}
