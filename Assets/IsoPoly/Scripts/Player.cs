﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IsoPoly
{
    public class Player : MonoBehaviour
    {
        public AnimationCurve m_AnimationCurve;
        public float m_MoveDuration = 2.0f;
        public enum MoveTypes { Simple, Smooth, Path}
        public MoveTypes m_MoveType;
        public enum PlayerStates { InActive, Active, Moving}
        public PlayerStates m_PlayerState;
        public GameObject m_PlayerOutline;

        public PlayerStates PlayerState {
            get { return m_PlayerState; }
            set {
                m_PlayerState = value;
                m_PlayerOutline.SetActive(m_PlayerState == PlayerStates.Active);//Show outline if active
            }
        }

        void Start()
        {
            PlayerState = PlayerStates.InActive;
        }

        void Update()
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if(Input.GetButtonDown("Fire1") && m_PlayerState == PlayerStates.InActive)//Make Active
            {
                if (Physics.Raycast(ray, out hit, 100))//Check if clicked on same grid location as player, if so make player active
                {
                    if(Mathf.RoundToInt(hit.point.x) == transform.position.x && Mathf.RoundToInt(hit.point.z) == transform.position.z)
                    {
                        PlayerState = PlayerStates.Active;
                        return;
                    }
                }
            }

            return;
            if (Input.GetButtonDown("Fire1") && m_PlayerState == PlayerStates.Active)//Move
            {
                if (Physics.Raycast(ray, out hit, 100))
                {
                    Vector3 destination = new Vector3(Mathf.RoundToInt(hit.point.x), 0, Mathf.RoundToInt(hit.point.z));
                    switch (m_MoveType)
                    {
                        case MoveTypes.Simple:
                            SimpleMove(destination);
                            break;
                        case MoveTypes.Smooth:
                            StartCoroutine(SmoothMove(destination));
                            break;
                    }
                }
            }
        }

        void SimpleMove(Vector3 destination)
        {
            Vector3 direction = destination - transform.position;
            transform.Translate(direction, Space.World);
        }

        IEnumerator SmoothMove(Vector3 destination)
        {
            Debug.Log("SmoothMove:" + destination);
            PlayerState = PlayerStates.Moving;
            float moveTimer = 0.0f;//how long the player has currently been moving
            Vector3 startPosition = transform.position;
            while (moveTimer <= m_MoveDuration)
            {
                transform.position = Vector3.Lerp(startPosition, destination, m_AnimationCurve.Evaluate(moveTimer / m_MoveDuration));
                moveTimer += Time.deltaTime;
                yield return null;
            }
            transform.position = destination;//Make sure it is exactly on the destination
            PlayerState = PlayerStates.InActive;
        }

        public IEnumerator SmoothMovePath(Vector3[] destinations)//Moves smoothly along a path
        {
            foreach (var d in destinations)
            {
                yield return SmoothMove(d);
            }
        }
    }
}