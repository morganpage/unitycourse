﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IsoPoly
{
    public class PathManager : MonoBehaviour
    {
        public LineRenderer m_LineRenderer;
        public GameObject[] m_Nodes; 
        public float m_PathY = 0.05f;//Y adjustment, otherwise looks a little low
        public Player m_Player;
        PathFind.Grid grid;
        List<Point> path;

        public void DefineBlocking(int width,int height,Point[] blockers)
        {
            bool[,] tilesmap = new bool[width, height];
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    tilesmap[x, y] = true;
                }
            }
            foreach (var b in blockers)
            {
                tilesmap[b.x, b.y] = false;
            }
            grid = new PathFind.Grid(width, height, tilesmap);
        }

        void SimplePath(Vector3 pathTo)
        {
            m_LineRenderer.SetPosition(0, new Vector3(m_Player.transform.position.x, m_PathY, m_Player.transform.position.z));
            m_LineRenderer.SetPosition(1, pathTo);
        }

        void AStarPath(Vector3 pathTo)
        {
            path = null;
            m_LineRenderer.gameObject.SetActive(false);
            foreach (var n in m_Nodes)
            {
                n.SetActive(false);
            }
            if (pathTo.x < 0 || pathTo.z < 0 || pathTo.x >= grid.GridSizeX || pathTo.z >= grid.GridSizeY) return;
            //AStar path
            path = PathFind.Pathfinding.FindPath(grid, new Point((int)m_Player.transform.position.x, (int)m_Player.transform.position.z), new Point((int)pathTo.x, (int)pathTo.z));
            if (path.Count == 0) return;//No route 
            Vector3 position = new Vector3(m_Player.transform.position.x, m_PathY, m_Player.transform.position.z);
            m_LineRenderer.SetPosition(0, position);
            m_Nodes[0].transform.position = position;
            m_Nodes[0].SetActive(true);
            m_LineRenderer.numPositions = path.Count+1;
            for (int i = 0; i < path.Count; i++)
            {
                position = new Vector3(path[i].x, m_PathY, path[i].y);
                m_LineRenderer.SetPosition(i+1, new Vector3(path[i].x, m_PathY, path[i].y));//path[i].x
                if (m_Nodes.Length > (i + 1))
                {
                    m_Nodes[i + 1].transform.position = position;
                    m_Nodes[i + 1].SetActive(true);

                }
            }
            m_LineRenderer.gameObject.SetActive(true);
        }

        void Update()
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            bool hitFloor = Physics.Raycast(ray, out hit, 100);
            if(m_Player.PlayerState != Player.PlayerStates.Active && m_LineRenderer.gameObject.activeSelf)//If player not active but still showing path, don't show path
            {
                m_LineRenderer.gameObject.SetActive(false);
                foreach (var n in m_Nodes)
                {
                    n.SetActive(false);
                }
            }
            if (hitFloor && m_Player.PlayerState == Player.PlayerStates.Active)
            {
                Vector3 destination = new Vector3(Mathf.RoundToInt(hit.point.x), m_PathY, Mathf.RoundToInt(hit.point.z));
                AStarPath(destination);
            }

            if (Input.GetButtonDown("Fire1") && path != null && path.Count != 0)
            {
                Vector3[] vpath = new Vector3[path.Count];
                for (int i = 0; i < path.Count; i++)
                {
                    vpath[i] = new Vector3(path[i].x, 0, path[i].y);
                }
                path = null;
                StartCoroutine(m_Player.SmoothMovePath(vpath));
            }
        }

    }
}