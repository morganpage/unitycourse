﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace IsoPoly
{
    public class GameManager : MonoBehaviour
    {
        public GameObject m_Levels;
        public PathManager m_PathManager;

        void Start()
        {
            m_Levels.GetComponent<LevelBuilder>().Build();
            Level level = m_Levels.GetComponent<Levels>().m_Levels[0];
            m_PathManager.DefineBlocking(level.Width, level.Height , level.GetPoints('^'));
        }
        
    }
}